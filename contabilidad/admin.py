# coding: utf8
from django.contrib import admin

# Register your models here.
from django.utils.html import format_html

from contabilidad.forms import CuentaForm, AsientoDetalleForm, AsientoForm, AsientoDetalleBaseInline, \
    EjercicioFiscalForm
from contabilidad.models import *
from django.contrib import messages


class PadreFilter(admin.SimpleListFilter):
    title = 'Cuenta Padre'
    parameter_name = 'padre'

    def lookups(self, request, model_admin):
        cuentas = Cuenta.objects.filter(cuenta_padre__isnull=True)
        lista = []
        for c in cuentas:
            lista.append([c.codigo,c])
        return lista

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(codigo__istartswith=self.value())
        return queryset


class CuentaAdmin(admin.ModelAdmin):
    list_filter = [PadreFilter,]
    form = CuentaForm
    search_fields = ['codigo','nombre']
    readonly_fields = ['asentable','padre']

    def padre(self,obj):
        if obj.pk:
            hijos = '<a href="/admin/contabilidad/cuenta/%s/change/">%s</a><br>'%(obj.cuenta_padre.pk,str(obj.cuenta_padre))
            print hijos
            return format_html(hijos)
        return ' '
    #
    # def has_change_permission(self, request, obj=None):
    #     self.readonly_fields = ['nombre','codigo','cuenta_padre','activo','asentable']
    #     return True

    def get_queryset(self, request):
        queryset = super(CuentaAdmin,self).get_queryset(request)
        return queryset.order_by('orden')

    def save_model(self, request, obj, form, change):
        if not change:
            obj.asentable = True
        if obj.cuenta_padre:
            obj.cuenta_padre.asentable = False
            obj.cuenta_padre.save()
        obj.save()

    def delete_model(self, request, obj):
        if obj.cuenta_padre:
            cuenta_padre = obj.cuenta_padre
            obj.delete()
            queryset = Cuenta.objects.filter(cuenta_padre=obj.cuenta_padre)
            if not queryset.exists():
                cuenta_padre.asentable = True
                cuenta_padre.save()



class AsientoDetalleInline(admin.TabularInline):
    model = AsientoDetalle
    form = AsientoDetalleForm
    formset = AsientoDetalleBaseInline


    def get_queryset(self, request):
       # self.readonly_fields = ['cuenta','debe','haber','observacion']
        return  super(AsientoDetalleInline, self).get_queryset(request)



class AsientoAdmin(admin.ModelAdmin):
    inlines = [AsientoDetalleInline]
    form = AsientoForm
    list_display = ['numero','fecha']
    readonly_fields = []

    def get_queryset(self, request):
        queryset = super(AsientoAdmin, self).get_queryset(request)
        return queryset.filter(ejercicio=get_ejercicio_fiscal())







class LibroDiarioAdmin(admin.ModelAdmin):
    list_display = ['detalle']
    search_fields = ['numero']
    list_display_links = None
    list_filter = ['fecha']
    actions = None

    def get_queryset(self, request):
        queryset = super(LibroDiarioAdmin,self).get_queryset(request)
        return queryset.order_by('pk')

    def get_queryset(self, request):
        queryset = super(LibroDiarioAdmin, self).get_queryset(request)
        return queryset.filter(ejercicio=get_ejercicio_fiscal()).order_by('numero')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False



    def detalle(self,obj):
        detalles = AsientoDetalle.objects.filter(asiento_id=obj.pk).order_by('-debe','-haber')
        tbody = ''
        for d in detalles:


            datos = {
                'cuenta1':d.debe > 0 and d.cuenta.nombre or '',
                'cuenta2': d.haber > 0 and 'A '+d.cuenta.nombre or '',
                'debe' : d.debe > 0 and d.debe or '',
                'haber' : d.haber > 0 and d.haber or ''
            }

            tbody +='''<tr>
                        <td></td>
                        <td> {cuenta1}</td>
                        <td>{cuenta2}</td>
                        <td style="text-align:right;">{debe}</td><td style="text-align:right;">{haber}</td>
                    </tr>'''.format(**datos)
        data = {
            'observacion':obj.observacion,
        }
        tbody+='''<tr>
                    <td></td>
                    <td colspan="2">{observacion}</td>
                    <td style="text-align:right;"><b></b></td>
                    <td style="text-align:right;"><b></b></td>
                </tr>'''.format(**data)

        tabla = '''<table >
                        <thead>
                            <th><a href="/admin/contabilidad/asiento/%s/change">%s</a></th>
                            <th colspan="2" style="text-align:center;width:50%%">%s</th>
                            <th style="text-align:center;width:25%%"></th>
                            <th style="text-align:center;width:25%%"></th></thead>
                        <tbody>
                            %s
                        </tbody>
                    </table>'''%(obj.pk,obj.fecha,obj.pk,tbody)
        return  format_html(tabla)

    detalle.short_description = 'Libro Diario'


class LibroMayorAdmin(admin.ModelAdmin):
    list_display = ['fecha','codigo','nombre','debe','haber']
    search_fields = ['numero']
    list_display_links = None
    list_filter = ['fecha']
    actions = None





class EjercicioFiscalAdmin(admin.ModelAdmin):
    list_display = ['anho','descripcion']
    form = EjercicioFiscalForm
    fields = ['anho','descripcion','activo','nro_asiento']
    readonly_fields = ['nro_asiento']

    def save_model(self, request, obj, form, change):

        if not change:
            obj.nro_asiento = 1
        queryset = EjercicioFiscal.objects.filter(activo = True)
        if not queryset.exists() and not obj.activo:
            message = u'Debido a que no hay Ejercicio Activo este pasa a ser el ejercicio activo.'
            messages.add_message(request=request,message=message,level=messages.WARNING)
            obj.activo = True
        elif queryset.exists() and obj.activo:
            for e in EjercicioFiscal.objects.filter(activo = True):
                e.activo = False
                e.save()

        if obj.activo:
            message = u'A partir de ahora solo se mostrarán datos correspondientes a este ejercicio.'
            messages.add_message(request=request, message=message, level=messages.WARNING)
        obj.save()


class ConfiguracionesAdmin(admin.ModelAdmin):
    list_display = ['codigo','descripcion','valor']

admin.site.register(EjercicioFiscal, EjercicioFiscalAdmin)
admin.site.register(Cuenta, CuentaAdmin)
admin.site.register(Asiento, AsientoAdmin)
admin.site.register(LibroDiario,LibroDiarioAdmin)
admin.site.register(LibroMayor,LibroMayorAdmin)