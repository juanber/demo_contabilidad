# coding: utf8
from __future__ import unicode_literals

import re

from django import forms
from dal import autocomplete
from django.core.exceptions import ValidationError
import re

from django.forms.models import BaseInlineFormSet
from django.template.defaultfilters import date

from contabilidad.models import *

ATTR_NUMERICO = {'style': 'text-align:right','class':'auto','data-a-sep':'.','data-a-dec':','}
ATTR_NUMERICO_RO = ATTR_NUMERICO.copy()


def codigo_valido(codigo):
	if re.match(r'[\d*\.]*',codigo):
		lista = codigo.split('.')
		for l in lista:
			if not re.match(r"^[1-9][0-9]*",l):
				return False
		return True
	return False

class CuentaForm(forms.ModelForm):
    class Meta:
        exclude = ['ultimo_codigo','orden']
        model = Cuenta
        #fields = ['nombre','codigo','cuenta_padre','tipo']
        widgets = {"cuenta_padre": autocomplete.ModelSelect2(url='/admin/contabilidad/cuenta-autocomplete/'),}


    def clean(self):
        codigo = self.cleaned_data['codigo']
        padre = self.cleaned_data.get("cuenta_padre")

        if self.instance and self.instance.pk:
            queryset = Cuenta.objects.filter(codigo=codigo).exclude(pk=self.instance.pk)
        else:
            queryset = Cuenta.objects.filter(codigo=codigo)

        if queryset.exists():
            self.add_error('codigo',u'Ya existe una cuenta con este código')

        if not padre and not codigo.isdigit():
            self.add_error('codigo', u'Si es una cuenta padre el código debe ser un número entero')

        if not codigo_valido(codigo):
            self.add_error('codigo', u'Formato incorrecto.')
        else:
            if padre and padre.codigo != '.'.join(codigo.split('.')[:-1]):
                self.add_error('codigo', u'Debe comenzar con el códido del padre.')



class AsientoForm(forms.ModelForm):
    class Meta:
        exclude = ['ejercicio']
        #fields = ['numero','fecha','observacion']
        model = Asiento

    numero = forms.CharField(disabled=True,
                                widget=forms.TextInput(attrs={'readonly':'readonly','style':'text-align:right'}))
    total_debe = forms.CharField(widget=forms.TextInput(attrs=ATTR_NUMERICO),
                                    disabled=True,initial=0)
    total_haber = forms.CharField(widget=forms.TextInput(attrs=ATTR_NUMERICO),
                                     disabled=True,initial=0)


    def __init__(self,*args, **kwargs):
        super(AsientoForm,self).__init__(*args, **kwargs)
        if not self.instance or not self.instance.pk:
            self.fields['numero'].initial = get_next_nro_asiento()

    def clean(self):
        res = super(AsientoForm, self).clean()
        if res['fecha'].year != get_ejercicio_fiscal().anho:
            raise ValidationError('La fecha del asiento no corresponde al ejercicio fiscal activo.', code='Invalido')



class AsientoDetalleBaseInline(BaseInlineFormSet):
    def clean(self):
        super(AsientoDetalleBaseInline, self).clean()
        total_debe = 0
        total_haber = 0
        borrar = False
        for form in self.forms:
            if not form.is_valid():
                return  # other errors exist, so don't bother
            if form.cleaned_data and not form.cleaned_data.get('DELETE'):
                total_debe += int(form.cleaned_data['debe']) if form.cleaned_data['debe'] else 0
                total_haber += int(form.cleaned_data['haber']) if form.cleaned_data['haber'] else 0
            if form.cleaned_data.get('DELETE'):
                borrar = True
        if total_debe != total_haber and borrar:
            raise ValidationError(u'No se puede eliminar el detalle porque el debe no coincidirá con el haber.', code='Invalido')

        if total_debe != total_haber:
            raise ValidationError('El debe no coincide con el haber.', code='Invalido')



class AsientoDetalleForm(forms.ModelForm):
    class Media:
        js = ('js/contabilidad/change_form.js','js/autoNumeric.js')


    class Meta:
        exclude = []
        model = AsientoDetalle
        widgets = {"cuenta": autocomplete.ModelSelect2(url='/admin/contabilidad/cuenta-autocomplete/'),}
    debe = forms.CharField(widget=forms.TextInput(attrs=ATTR_NUMERICO)
                              ,label='Debe',required=False)
    haber = forms.CharField(widget=forms.TextInput(attrs=ATTR_NUMERICO)
                               ,label='Haber',required=False)

    def clean(self):
        res = super(AsientoDetalleForm, self).clean()
        debe = int(res['debe']) if res['debe'] else 0
        haber = int(res['haber']) if res['haber'] else 0
        print res,not debe and not haber , debe and haber
        if not debe and not haber or debe and haber:
            raise ValidationError('Se debe completar un campo, debe o haber', code='Invalido')



class EjercicioFiscalForm(forms.ModelForm):

    def clean(self):
        res = super(EjercicioFiscalForm, self).clean()
        queryset = EjercicioFiscal.objects.all()
        if not self.instance or not self.instance.pk:
            if queryset.filter(anho=res['anho']).exists():
                raise ValidationError(u'Ya existe un ejercicio para este año', code='Invalido')
