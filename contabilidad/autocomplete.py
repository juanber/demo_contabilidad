from dal import autocomplete
from django.db.models import Q
from .models import Cuenta

class CuentaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Cuenta.objects.none()
        qs = Cuenta.objects.filter(activo=True)
        if self.q:
            qs = qs.filter( Q(nombre__icontains=self.q) | Q(codigo__istartswith=self.q) )
        return qs.order_by('orden')
