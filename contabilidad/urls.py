from django.conf.urls import patterns, include, url
from contabilidad.autocomplete import *
from contabilidad.views import *

urlpatterns = [
    url(
        'cuenta-autocomplete/$',
        CuentaAutocomplete.as_view(),
        name='cuenta-autocomplete',
    ),
]

