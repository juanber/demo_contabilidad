# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-28 07:10
from __future__ import unicode_literals

from django.db import migrations, models
from django.db import migrations, models
import django.db.models.deletion



class Migration(migrations.Migration):

    dependencies = [
        ('contabilidad', '0022_ejerciciofiscal_nro_asiento'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asiento',
            name='fecha',
            field=models.DateField(auto_now_add=True),
        ),
        migrations.AddField(
            model_name='librodiario',
            name='ejercicio',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE,
                                    to='contabilidad.EjercicioFiscal'),
        ),

    ]
