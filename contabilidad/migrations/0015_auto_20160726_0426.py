# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-26 04:26
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('contabilidad', '0014_auto_20160725_1827'),
    ]

    operations = [

        migrations.AlterField(
            model_name='asiento',
            name='fecha',
            field=models.DateField(default=datetime.datetime(2016, 7, 26, 4, 26, 42, 335342, tzinfo=utc)),
        ),
    ]
