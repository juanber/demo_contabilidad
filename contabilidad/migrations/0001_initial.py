# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-25 01:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cuenta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
                ('codigo', models.CharField(max_length=30)),
                ('tipo', models.CharField(choices=[('AC', 'Activo'), ('PA', 'Pasivo'), ('PN', 'Patrimonio Neto'), ('IN', 'Ingresos'), ('EG', 'Egresos')], max_length=15)),
                ('cuenta_padre', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='contabilidad.Cuenta')),
            ],
        ),
    ]
