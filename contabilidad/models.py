# coding: utf8
from __future__ import unicode_literals

from datetime import datetime

from django.core.serializers.python import _get_model
from django.utils import timezone
from django.db import models
# Create your models here.
from django.db.models.aggregates import Max
from django.db.models.deletion import PROTECT, CASCADE
from django.views.generic.dates import timezone_today

TIPOS_DE_CUENTA = (
    ('AC', 'Activo'),
    ('PA', 'Pasivo'),
    ('PN', 'Patrimonio Neto'),
    ('IN', 'Ingresos'),
    ('EG', 'Egresos'),
)


def get_codigo_orden(codigo):
    abc = 'abcdefghijklmnopqrstuvwxyz'
    orden = ''
    for c in codigo.split('.'):
        indice = int(c)
        if indice <= 26:
			orden+=abc[indice-1]
        else:
			orden += ''.join(['z' for i in range(indice/26)])
			orden += abc[(indice%26)-1]
    return orden


class EjercicioFiscal(models.Model):
    class Meta:
        verbose_name_plural = 'Ejercicios Fiscales'
    anho = models.IntegerField(unique=True)
    descripcion = models.CharField(max_length=100)
    nro_asiento = models.IntegerField(default=0)
    activo = models.BooleanField(default=False)

    def __unicode__(self):
        return  str(self.anho)

    def save(self,*args,**kwargs):
        print args,kwargs
        return super(EjercicioFiscal, self).save(args,kwargs)

    def set_next_nro_asiento(self):
        self.nro_asiento+=1
        self.save()



def get_next_nro_asiento():
    ejercicio = EjercicioFiscal.objects.filter(activo=True)
    return ejercicio.first().nro_asiento if ejercicio.exists() else 1

def get_ejercicio_fiscal():
    ejercicios = EjercicioFiscal.objects.filter(activo=True)
    return ejercicios.first() if ejercicios.exists() else None


class Cuenta(models.Model):
    nombre = models.CharField(max_length=200)
    codigo = models.CharField(max_length=30,help_text=u"Utilice el siguiente formato: <em>x.x.x</em>. No agregue cero antes de un número.")
    cuenta_padre = models.ForeignKey('Cuenta',null=True,blank=True,on_delete=PROTECT)
    orden = models.CharField(max_length=90)
    activo = models.BooleanField(default=True)
    asentable = models.BooleanField(default=True)

    def __str__(self):
        return '%s - %s'%(self.codigo,self.nombre)

    def save(self, force_insert=False, force_update=False, using=None,update_fields=None):
        self.orden = get_codigo_orden(self.codigo)
        return super(Cuenta,self).save( force_insert=False, force_update=False, using=None,update_fields=None)

class Asiento(models.Model):
    fecha = models.DateField(default=datetime.now)
    observacion = models.CharField(max_length=100, null=True)
    numero = models.IntegerField(blank=True, null=True)
    total_debe = models.IntegerField(default=0)
    total_haber = models.IntegerField(default=0)
    ejercicio = models.ForeignKey(EjercicioFiscal,null=True)

    def __unicode__(self):
        return '%s - %s'%(self.numero,unicode(self.fecha))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.pk:
            self.ejercicio = get_ejercicio_fiscal()
            self.numero = self.ejercicio.nro_asiento
            self.ejercicio.set_next_nro_asiento()
        return super(Asiento, self).save(force_insert=False, force_update=False, using=None, update_fields=None)



class AsientoDetalle(models.Model):
    asiento = models.ForeignKey(Asiento,on_delete=CASCADE)
    cuenta = models.ForeignKey(Cuenta,on_delete=PROTECT)
    debe = models.IntegerField(null=True,blank=True)
    haber = models.IntegerField(null=True,blank=True)
    observacion = models.CharField(max_length=100, null=True)

    def __str__(self):
        return ''

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.debe:
            self.debe = 0

        if not self.haber:
            self.haber = 0
        return super(AsientoDetalle, self).save(force_insert=False, force_update=force_update, using=None, update_fields=None)


class LibroDiario(models.Model):
    class Meta:
      managed = False
      db_table = 'contabilidad_librodiario'
      verbose_name_plural = 'Libro Diario'
      verbose_name  = 'Libro Diario'


      #sql = 'CREATE VIEW contabilidad_librodiario AS SELECT * FROM contabilidad_asiento'

    fecha = models.DateField()
    observacion = models.CharField(max_length=100, null=True)
    numero = models.IntegerField(blank=True, null=True)
    total_debe = models.IntegerField(default=0)
    total_haber = models.IntegerField(default=0)
    ejercicio = models.ForeignKey(EjercicioFiscal,null=True)

class LibroMayor(models.Model):
    class Meta:
        managed = False
        db_table = 'contabilidad_libromayor'
        verbose_name_plural = 'Libro Mayor'
        verbose_name = 'Libro Mayor'
        # sql = '''create view contabilidad_libromayor as
        #           select asiento.fecha,asiento.observacion,cuenta.nombre,cuenta.codigo,
        #           sum(detalle.debe) as debe, sum(detalle.haber) as haber
        #          from contabilidad_asientodetalle detalle
        #             join contabilidad_asiento asiento on asiento.id = detalle.asiento_id
        #             join contabilidad_cuenta cuenta on cuenta.id = detalle.cuenta_id
        #             group by asiento.fecha,asiento.observacion,cuenta.nombre,cuenta.codigo
        #                 '''
    fecha = models.DateField()
    observacion = models.CharField(max_length=100, null=True)
    nombre = models.CharField(max_length=200)
    codigo = models.CharField(max_length=30)
    debe = models.IntegerField(default=0)
    haber = models.IntegerField(default=0)