/**
 * Created by juanber on 25/07/16.
 */

function setAll(){
    $('#id_total_debe').autoNumeric('set', get_total_debe());
    $('#id_total_haber').autoNumeric('set', get_total_haber());
}

django.jQuery(document).ready(function() {

    $('.auto').autoNumeric('init', {aSign:' GS', pSign:'s',mDec:0 });

    setAll();

    $('input[name$=-debe]').keyup(function () {
        $('#id_total_debe').autoNumeric('set', get_total_debe());
    });
    $('input[name$=-haber]').keyup(function () {
       $('#id_total_haber').autoNumeric('set', get_total_haber());
    });

    $("#asiento_form").submit(function( event ) {
       $('.auto').each(function () {
           $(this).val($(this).autoNumeric('get'));
       });
      //event.preventDefault();
    });
});

django.jQuery(document).on('formset:removed', function(event, $row, formsetName) {
   setAll();
});

function get_total_debe() {
    var total = 0;
     $('input[name$=-debe]').each(function () {
         if (this.value)
            total+=parseInt($(this).autoNumeric('get'));
        });
    return total;
}

function get_total_haber() {
  var total = 0;
     $('input[name$=-haber]').each(function () {
         if (this.value)
            total+=parseInt($(this).autoNumeric('get'));
        });
    return total;
}
