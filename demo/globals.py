

def clean_sep(valor):
    '''
    :param valor: Un string que representa un numero con separador de miles.
    :return: un entero sin separador de miles
    '''
    return int(valor.replace(".", "").replace(",", ".").replace(' GS',''))
